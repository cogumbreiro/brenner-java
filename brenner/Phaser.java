package brenner;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Collection;
import java.util.Map;

public class Phaser {
    public class Participant {
        private final AtomicInteger phase;
        
        private Participant(int value) {
            phase = new AtomicInteger(value);
        }
        
        public Participant register() {
            Participant other = new Participant(phase.get());
            Phaser.this.register(other);
            return other;
        }
        
        public void await() throws Exception {
            Phaser.this.await(phase.get());
        }
        protected void unsafeAwait() {
        	try {
        		await();
        	} catch (Exception e) {
        		throw new RuntimeException(e);
        	}
        }
        
        public void await(int phase) throws Exception {
            Phaser.this.await(phase);
        }
        
        protected void unsafeAwait(int phase) {
        	try {
        		await(phase);
        	} catch (Exception e) {
        		throw new RuntimeException(e);
        	}
        }
        
        public int advance() {
            int value = phase.incrementAndGet();
            Phaser.this.advance(this);
            return value;
        }
        /**
         * Deregisters the current participant.
         */
        public void deregister() {
            Phaser.this.deregister(this);
        }
        /**
         * Returns a phaser so tasks can observe it
         */
        public Phaser getPhaser() {
            return Phaser.this;
        }
    }
    // marks the current state of the phaser
    private final EventStream state;
    private final Collection<AtomicInteger> participants = new CopyOnWriteArrayList<>();
    
    private Phaser(EventStream stream) {
        this.state = stream;
    }
    
    protected void notifyBlocked() {
        state.trigger();
    }
    
    private void advance(Participant part) {
        notifyBlocked();
    }
    
    private void register(Participant part) {
        participants.add(part.phase);
        notifyBlocked();
    }
    
    private void deregister(Participant part) {
        participants.remove(part.phase);
        notifyBlocked();
    }
    
    /**
     * Returns true when the operation is successful, ie all registered
     * participants have at least the given phase
     */
    private boolean nonblockingAwait(int phase) {
        for (AtomicInteger evt : participants) {
            if (evt.get() < phase) return false;
        }
        return true;
    }
    
    public void await(int phase) throws Exception {
        for (;;) {
            long currState = state.getEvent();
            if (nonblockingAwait(phase)) {
                return;
            }
            // check for the next of phasers change
            state.await(currState);
        }
    }
    
    private Participant init(int phase) {
        Participant part = new Participant(phase);
        register(part);
        return part;
    }
    
    public static Participant createJPhaser(int phase) {
        return createPhaser(new JEventStream(), phase);
    }

    public static Participant createKPhaser(int phase) {
        return createPhaser(new KEventStream(), phase);
    }

    public static Participant createLPhaser(int phase) {
        return createPhaser(new LEventStream(), phase);
    }

    public static Participant createMPhaser(int phase) {
        return createPhaser(new MEventStream(), phase);
    }
    
    public static Participant createPhaser(EventStream stream, int phase) {
        return new Phaser(stream).init(phase);
    }

    public static Participant createHjPhaser(int phase) {
        return new Phaser(new HjEventStream()).init(phase);
    }
}

