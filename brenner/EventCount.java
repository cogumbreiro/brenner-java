package brenner;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

public class EventCount {
    private static class Sync extends AbstractQueuedSynchronizer {
        public Sync(int evt) {
            setState(evt);
        }
        
        // called from await
        protected int tryAcquireShared(int evt) {
            return getState() >= evt ? 1 : -1;
        }

        // called from advance
        protected boolean tryReleaseShared(int dummy) {
            int st = getState();
            return compareAndSetState(st, st + 1);
        }
        
        int get() {
            return getState();
        }
    }
    
    private final Sync sync;
    
    public EventCount() {
        this(0);
    }
    
    public EventCount(int evt) {
        sync = new Sync(evt);
    }
    
    public void await(int evt) throws InterruptedException {
        sync.acquireSharedInterruptibly(evt);
    }
    
    public void advance() {
        sync.releaseShared(0);
    }
    
    public int get() {
        return sync.get();
    }
    
}
