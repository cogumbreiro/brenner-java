package brenner;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;

public class LEventStream implements EventStream {
    private static class EventCount extends AbstractQueuedSynchronizer {
        boolean isSignalled() { return getState() != 0; }
        
        protected int tryAcquireShared(int evt) {
            return getState() >= evt ? 1 : -1;
        }

        protected boolean tryReleaseShared(int dummy) {
            int st = getState();
            return compareAndSetState(st, st + 1);
        }
        
        public void signal() {
            releaseShared(0);
        }
        
        public void await(int evt) throws InterruptedException {
            acquireSharedInterruptibly(evt);
        }
        
        public int get() {
            return getState();
        }
    }
    private final EventCount sync = new EventCount();
    
    public void trigger() {
        sync.signal();
    }
    
    @Override
    public long getEvent() {
        return sync.get() + 1;
    }
    
    @Override
    public void await(final long event) throws InterruptedException {
        sync.await((int) event);
    }
}
