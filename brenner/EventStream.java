package brenner;

public interface EventStream {
    void trigger();
    
    long getEvent();
    
    void await(long event) throws Exception;
}
