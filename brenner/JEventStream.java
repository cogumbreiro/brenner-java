package brenner;

public class JEventStream implements EventStream {
    private final Object lock = new Object();
    private volatile long state = 0;
    
    public void trigger() {
        synchronized(lock) {
            state++;
            lock.notifyAll();
        }
    }
    
    @Override
    public synchronized long getEvent() {
        return state;
    }
    
    @Override
    public void await(final long event) throws InterruptedException {
        synchronized(lock) {
            while (event == state) {
                lock.wait();
            }
        }
    }
}
