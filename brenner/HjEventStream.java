package brenner;

import edu.rice.hj.api.HjEventCount;
import static edu.rice.hj.ModuleE.newEventCount;
import edu.rice.hj.api.SuspendableException;

public class HjEventStream implements EventStream {
    private final HjEventCount state = newEventCount();
    
    public void trigger() {
        state.advance();
    }
    
    @Override
    public long getEvent() {
        return state.read() + 1;
    }
    
    @Override
    public void await(long event) throws SuspendableException {
        state.await(event);
    }
}
