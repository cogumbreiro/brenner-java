package brenner;

import java.util.concurrent.CyclicBarrier;

import edu.rice.hj.api.HjPhaser;
//import edu.rice.hj.api.HjDataDrivenFuture;
//import edu.rice.hj.api.HjRuntime;
import edu.rice.hj.runtime.config.HjConfiguration;
import static edu.rice.hj.Module1.*;
import static edu.rice.hj.api.HjPhaserMode.*;

public class Main2 {
	private static final int TASKS = 6;

	private static void runBrenner(boolean useHj, final long count) {
		Phaser.Participant driver = useHj ? Phaser.createHjPhaser(0) : Phaser.createKPhaser(0);
		for (int t = 0; t < TASKS; t++) {
			final Phaser.Participant t1 = driver.register();
			async(() -> {
				int z = 0;
				for (int i = 0; i < count; i++) {
					z *= i + 2;
					t1.unsafeAwait(t1.advance());
				}
			});
		}
		driver.deregister();
	}

	private static void runHjBrenner(final long count) {
		runBrenner(true, count);
	}

	private static void runJBrenner(final long count) {
		runBrenner(false, count);
	}

	private static void runHj(final long count) {
		final HjPhaser ph = newPhaser(SIG_WAIT);
		for (int t = 0; t < TASKS; t++)
			asyncPhased(ph.inMode(SIG_WAIT), () -> {
				int z = 0;
				for (int i = 0; i < count; i++) {
					z *= i + 2;
					ph.doNext();
				}
			});
		ph.drop();
	}

	private static void runJ(final long count) {
		final CyclicBarrier ph = new CyclicBarrier(TASKS);
		for (int t = 0; t < TASKS; t++)
			async(() -> {
				int z = 0;
				for (int i = 0; i < count; i++) {
					z *= i + 2;
					try {
						ph.await();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
	}

	private static void runP(final long count) {
		final java.util.concurrent.Phaser ph = new java.util.concurrent.Phaser(TASKS);
		for (int t = 0; t < TASKS; t++)
			async(() -> {
				int z = 0;
				for (int i = 0; i < count; i++) {
					z *= i + 2;
					try {
						ph.arriveAndAwaitAdvance();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
	}
	
	public static void main(final String[] args) {
		final int COUNT = 100000;
		launchHabaneroApp(() -> {
			long start, elapsed;

			start = System.currentTimeMillis();
			finish(() -> runJBrenner(COUNT));
			elapsed = System.currentTimeMillis() - start;
			System.out.println("JBrenner: " + elapsed);

			start = System.currentTimeMillis();
			finish(() -> runHj(COUNT));
			elapsed = System.currentTimeMillis() - start;
			System.out.println("Hj: " + elapsed);

			start = System.currentTimeMillis();
			finish(() -> runJ(COUNT));
			elapsed = System.currentTimeMillis() - start;
			System.out.println("CyclicBarriers: " + elapsed);

			start = System.currentTimeMillis();
			finish(() -> runP(COUNT));
			elapsed = System.currentTimeMillis() - start;
			System.out.println("Phasers: " + elapsed);
			/*
			 * start = System.currentTimeMillis(); finish( () ->
			 * runHjBrenner(COUNT) ); elapsed = System.currentTimeMillis() -
			 * start; System.out.println("HjBrenner: " + elapsed);
			 */
		});

	}
}
