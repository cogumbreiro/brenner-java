package brenner;

import edu.rice.hj.api.HjPhaser;
//import edu.rice.hj.api.HjDataDrivenFuture;
//import edu.rice.hj.api.HjRuntime;
import edu.rice.hj.runtime.config.HjConfiguration;
import static edu.rice.hj.Module1.*;
import static edu.rice.hj.api.HjPhaserMode.*;

public class Main {
	private static final int PRODUCERS = 500;
	private static final int CONSUMERS = 100;

	private static void runBrenner(boolean useHj, final long count) {
		Phaser.Participant driver = useHj ? Phaser.createHjPhaser(0) : Phaser.createLPhaser(0);
		Phaser ph = driver.getPhaser();
		for (int t = 0; t < PRODUCERS; t++) {
			final Phaser.Participant t1 = driver.register();
			async(() -> {
				int z = 0;
				for (int i = 0; i < count; i++) {
					z *= i + 2;
					t1.advance();
				}
			});
		}
		driver.deregister();

		for (int t = 0; t < CONSUMERS; t++)
			async(() -> {
				try {
					int phase = 1;
					int z = 0;
					for (int i = 0; i < count; i++) {
						z *= i + 3;
						ph.await(phase);
						phase++;
					}
				} catch (Exception e) {
				}
			});
	}

	private static void runHjBrenner(final long count) {
		runBrenner(true, count);
	}

	private static void runJBrenner(final long count) {
		runBrenner(false, count);
	}

	private static void runHj(final long count) {
		final HjPhaser ph = newPhaser(SIG_WAIT);
		for (int t = 0; t < PRODUCERS; t++)
			asyncPhased(ph.inMode(SIG), () -> {
				int z = 0;
				for (int i = 0; i < count; i++) {
					z *= i + 2;
					ph.signal();
				}
			});

		for (int t = 0; t < CONSUMERS; t++)
			asyncPhased(ph.inMode(WAIT), () -> {
				try {
					int phase = 1;
					int z = 0;
					for (int i = 0; i < count; i++) {
						z *= i + 3;
						ph.doWait();
						phase++;
					}
				} catch (Exception e) {
				}
			});
	}

	public static void main(final String[] args) {
		final int COUNT = 100000;
		launchHabaneroApp(() -> {
			long start, elapsed;

			start = System.currentTimeMillis();
			finish(() -> runJBrenner(COUNT));
			elapsed = System.currentTimeMillis() - start;
			System.out.println("JBrenner: " + elapsed);

			start = System.currentTimeMillis();
			finish(() -> runHj(COUNT));
			elapsed = System.currentTimeMillis() - start;
			System.out.println("Hj: " + elapsed);
			/*
			 * start = System.currentTimeMillis(); finish( () ->
			 * runHjBrenner(COUNT) ); elapsed = System.currentTimeMillis() -
			 * start; System.out.println("HjBrenner: " + elapsed);
			 */
		});

	}
}
