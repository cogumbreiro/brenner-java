package brenner;

public class MEventStream implements EventStream {
    private final EventCount state = new EventCount();
    
    public void trigger() {
        state.advance();
    }
    
    @Override
    public long getEvent() {
        return state.get() + 1;
    }
    
    @Override
    public void await(long event) throws InterruptedException {
        state.await((int) event);
    }
}
