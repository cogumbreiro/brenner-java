package brenner;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;

public class KEventStream implements EventStream {
    private final AtomicLong state = new AtomicLong();
    private Lock lock = new ReentrantLock();
    private Condition changed = lock.newCondition();
    
    public void trigger() {
        state.incrementAndGet();
        lock.lock();
        try {
            changed.signalAll();
        } finally {
            lock.unlock();
        }
    }
    
    @Override
    public long getEvent() {
        return state.get();
    }
    
    @Override
    public void await(final long event) throws InterruptedException {
        if (event == state.get()) {
            lock.lock();
            try {
                while (event == state.get()) {
                    changed.await();
                }
            } finally {
                lock.unlock();
            }
        }
    }
}
