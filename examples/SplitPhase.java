package examples;

import java.util.Random;
import java.util.Arrays;
import brenner.Phaser;
import brenner.Phaser.Participant;

public class SplitPhase {
    public static final int N = 3; // number of tasks
    public static final int M = 100; // number of iterations
    public static void main(String[] args) {
        final double P[] = new double[N];
        // Initialize:
        Random rnd = new Random();
        for (int i = 0; i < N; i++) {
            P[i] = rnd.nextDouble();
        }
        // The actual example:
        final Participant p = Phaser.createJPhaser(0); // start at phase 0
        for (int _i = 0; _i < N; _i++) {
            final int i = _i;
            // In Java, we do not need to create task names, as they are
            // created automatically.
            final Participant t_p = p.register(); // reg(t, p)
            new Thread(() -> { // fork(t,...)
                try {
                    for (int k=1; k <= M; k++) {
                        double l = P[(i + N -1) % N];
                        double r = P[(i+1) % N];
                        System.out.println(Arrays.toString(P));
                        t_p.advance();
                        double tmp = (l + r) / 2;
                        t_p.await();
                        System.out.println(Arrays.toString(P));
                        P[i] = tmp;
                        t_p.advance();
                        t_p.await();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }).start();
        }
        p.deregister();
    }
}
