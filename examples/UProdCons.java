package examples;

import java.util.Random;
import java.util.Arrays;
import brenner.Phaser;
import brenner.Phaser.Participant;

public class UProdCons {
    public static final int N = 3; // number of tasks
    public static final int M = 100; // number of iterations
    public static void fork(Runnable r) {
        Thread t = new Thread(r);
        t.start();
    }
    
    private static int log2(int x) {
        return (int)(Math.log(x)/Math.log(2)+1e-10);
    }
    
    public static void prefixSum1() {
        final int N = 5;
        final int A[] = {5,8,7,2,3};
        final int S[] = new int[N];
        final int J = log2(N);
        System.out.println(J);
        for (int j = 0; j < J; j *= 2) {
            for (int i = j; i < N; i++) {
                System.out.println("S[" + i + "] = A[" + i + "] + A[" + (i - 1) + "];");
                S[i] = A[i] + (i - 1 >= 0 ? A[i - 1] : 0);
            }
        }
        System.out.println(Arrays.toString(S));
    }
    
    public static void main(String[] args) {
        /*
        final double P[] = new double[N];
        // Initialize:
        Random rnd = new Random();
        for (int i = 0; i < N; i++) {
            P[i] = rnd.nextDouble();
        }
        // The actual example:
        final Participant p = Phaser.createJPhaser(0); // start at phase 0
        for (int _i = 0; _i < N; _i++) {
            final int i = _i;
            // In Java, task names are created and treated internally.
            final Participant t_p = p.register(); // reg(t, p)
            fork(() -> { // fork(t,...)
                try {
                    for (int k=1; k <= M; k++) {
                        double l = P[(i + N -1) % N];
                        double r = P[(i+1) % N];
                        t_p.advance();
                        double tmp = (l + r) / 2;
                        t_p.await();
                        P[i] = tmp;
                        t_p.advance();
                        t_p.await();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            });
        }
        p.deregister();
        */
        prefixSum1();
    }
}
