HJLIB ?= hjlib.jar


JAVA ?= java
JAVAC ?= javac
JFLAGS = -cp $(HJLIB):. 

HJLIB_VERSION = 0.1.4
HJLIB_URL = http://www.cs.rice.edu/~vs3/hjlib/code/maven-repo/habanero-java-lib/habanero-java-lib/$(HJLIB_VERSION)-SNAPSHOT/habanero-java-lib-$(HJLIB_VERSION)-SNAPSHOT.jar

BIN = \
  brenner/EventStream.class \
  brenner/HjEventStream.class \
  brenner/Phaser.class \
  brenner/JEventStream.class \
  brenner/KEventStream.class \
  brenner/LEventStream.class \
  brenner/MEventStream.class \
  brenner/Main.class \
  brenner/Main2.class

all: hjlib.jar brenner.jar

hjlib.jar:
	wget $(HJLIB_URL) -O hjlib.jar

brenner.jar: $(BIN) hjlib.jar
	jar cf brenner.jar brenner/*.class

%.class: %.java
	$(JAVAC) $(JFLAGS) $<

run: brenner/Main.class
	$(JAVA) $(JFLAGS) brenner.Main

clean:
	rm -f brenner/*.class
